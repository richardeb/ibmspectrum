/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {
	
	 	/*
	* flexslider
	*/ 
	jQuery('.flexslider').flexslider( {
		animation: 'slide',
		animationSpeed: 1200,
		animationLoop: false,
		controlNav: true,
		prevText: "<p>Back <br/>to Dr. Delay</p>",      //String: Set the text for the "previous" directionNav item
		nextText: "<p>Learn About Lolo</p>",		//String: Set the text for the "next" directionNav item	
		slideshowSpeed: 99000
		
	});
}); /* end of as page load scripts */