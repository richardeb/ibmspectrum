<?php
function form_mailer() {
/* These are the variable that tell the subject of the email and where the email will be sent.*/

$emailSubject = 'LoLo Latency Free Gift Code!';
$mailto = 'msalley@iridiangroup.com';

/* These will gather what the user has typed into the fieled. */

$nameField = 'Sirius Computer Solutions';
$emailField = 'info@iridiangroup.com ';
$codeField = $_POST['giftcode'];

/* This takes the information and lines it up the way you want it to be sent in the email. */
	
$body = <<<EOD
<br><hr><br>
Code: $codeField <br>
EOD;
	
$headers = "From: $nameField\r\n"; // This takes the email and displays it as who this email is from.
$headers .= "Content-type: text/html\r\n"; // This tells the server to turn the coding into the text.

	if($codeField != '') {
		
		return mail($mailto, $emailSubject, $body, $headers); // This tells the server what to send.
		
	}else {
		
		return false;
		
	}
	
}
if(isset($_POST['submitting_form']) && $_POST['submitting_form'] == 1){
	$form_success = form_mailer();
	if($form_success){
		header('Location: '.$_SERVER['PHP_SELF'].'?form_success=true#free_gift_sidebar'); 
		die;
	}
}
?>	

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Home | Sirius Computer Solutions</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta property="og:title" content="Home">
        <meta property="og:type" content="article">
        <meta property="og:url" content="http://www.siriuscom.com/">
        <meta property="og:site_name" content="Sirius Computer Solutions">
        <meta property="og:description" content="Solutions integration provided by experienced and certified technology professionals    
		Solutions: Comprehensive, integrated technology solutions  
		Because our clients need more than just discrete hardware, software and services offerings, we&nbsp;offer complete solutions that help them overcome their most difficult business and IT challenges. Every">
		<meta property="og:image" content="http://www.siriuscom.com/wp-content/uploads/2016/02/SiriusLogo240x79.png">		
		
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        
        <!-- Place favicon.ico in the root directory -->
        
        <link rel="shortcut icon" href="http://www.siriuscom.com/wp-content/uploads/2016/02/Favicon.png" type="image/x-icon">
        
         <!-- fonts -->
		<link rel="stylesheet" id="ubermenu-lato-css" href="//fonts.googleapis.com/css?family=Lato%3A%2C300%2C400%2C700&amp;ver=4.4.3" type="text/css" media="all">
		<link rel="stylesheet" id="avada-google-fonts-css" href="http://fonts.googleapis.com/css?family=PT+Sans%3A400%2C400italic%2C700%2C700italic%7CAntic+Slab%3A400%2C400italic%2C700%2C700italic&amp;subset=latin&amp;ver=4.4.3" type="text/css" media="all">
		<link rel="stylesheet" id="fontawesome-css" href="http://www.siriuscom.com/wp-content/themes/Avada/assets/fonts/fontawesome/font-awesome.css?ver=1.0.0" type="text/css" media="all">		  
		
		<!-- ubermenu & animation styles -->
		
		<link rel="stylesheet" id="avada-animations-css" href="http://www.siriuscom.com/wp-content/themes/Avada/animations.css?ver=1.0.0" type="text/css" media="all">				
		<link rel="stylesheet" id="ubermenu-css" href="http://www.siriuscom.com/wp-content/plugins/ubermenu/pro/assets/css/ubermenu.min.css?ver=3.2.1.1" type="text/css" media="all">
		<link rel="stylesheet" id="ubermenu-trans-black-hov-css" href="http://www.siriuscom.com/wp-content/plugins/ubermenu/pro/assets/css/skins/trans_black_hover.css?ver=4.4.3" type="text/css" media="all">				
		<link rel="stylesheet" id="ubermenu-font-awesome-css" href="http://www.siriuscom.com/wp-content/plugins/ubermenu/assets/css/fontawesome/css/font-awesome.min.css?ver=4.3" type="text/css" media="all">	
				
		
		<!-- load jquery -->
		
		<script type="text/javascript" src="http://www.siriuscom.com/wp-includes/js/jquery/jquery.js?ver=1.11.3"></script>
		<script type="text/javascript" src="http://www.siriuscom.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>
		    
		<!-- theme styles -->   
		        
        <link rel="stylesheet" href="css/avada.css">        
        <link rel="stylesheet" href="css/ubermenu-custom-generated.css">      
        <link rel="stylesheet" href="css/avada-inline.css?ver=1.0.0">            
        <link rel="stylesheet" href="css/style.css?ver=1.0.0">
        
        <!-- slider styles -->   
        
        <link rel="stylesheet" id="flexslider-stylesheet-css" href="css/flexslider.css" type="text/css" media="all">
        
        <!-- load scripts --> 
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script type="text/javascript" src="js/vendor/jquery.flexslider.js"></script>
    </head>
    <body class="fusion-body">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<div class="fusion-header-wrapper" style="position: relative; z-index: 100; height: 185px; "> <!-- height with menu = 232px -->
			<div class="fusion-header-v4 fusion-logo-left fusion-sticky-menu-1 fusion-sticky-logo-1 fusion-mobile-logo-1 fusion-mobile-menu-design-modern fusion-sticky-menu-only fusion-header-menu-align-left">
													
				<div class="fusion-secondary-header">
					<div class="fusion-row">
						<div class="fusion-alignleft">
							<div class="fusion-contact-info">Contact Us Today! 800-460-1237<span class="fusion-header-separator">|</span><a href="mailto:info@siriuscom.com" target="_blank">info@siriuscom.com</a>
								
							</div>
						</div>
						<div class="fusion-alignright">
							<div class="fusion-social-links-header">
								<div class="fusion-social-networks">
									<div class="fusion-social-networks-wrapper">
										<a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#ffffff;" target="_blank" href="http://www.facebook.com/siriuscomputersolutions" data-placement="bottom" data-title="Facebook" data-toggle="tooltip" title="" data-original-title="Facebook"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#ffffff;" target="_blank" href="https://twitter.com/SiriusNews" data-placement="bottom" data-title="Twitter" data-toggle="tooltip" title="" data-original-title="Twitter"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube" style="color:#ffffff;" target="_blank" href="http://www.youtube.com/siriusnewscast" data-placement="bottom" data-title="Youtube" data-toggle="tooltip" title="" data-original-title="Youtube"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-googleplus fusion-icon-googleplus" style="color:#ffffff;" target="_blank" href="https://plus.google.com/u/0/b/112331788911174015967/112331788911174015967/posts" data-placement="bottom" data-title="Google+" data-toggle="tooltip" title="" data-original-title="Google+"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin fusion-last-social-icon" style="color:#ffffff;" target="_blank" href="http://www.linkedin.com/company/sirius-computer-solutions" data-placement="bottom" data-title="Linkedin" data-toggle="tooltip" title="" data-original-title="Linkedin"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fusion-header-sticky-height" style="display: none;"></div>
				<div class="fusion-sticky-header-wrapper" style="height: 140px;"> <!-- height with menu = 187px --><!-- start fusion sticky header wrapper -->
					<div class="fusion-header fusion-header-backface">
						<div class="fusion-row">
					
							<div class="fusion-logo" data-margin-top="0px" data-margin-bottom="0px" data-margin-left="0px" data-margin-right="0px">
								
								<a class="fusion-logo-link" href="http://www.siriuscom.com">
									<img src="//www.siriuscom.com/wp-content/uploads/2016/02/SiriusLogo240x79.png" width="113" height="24" alt="Sirius Computer Solutions" class="fusion-logo-1x fusion-standard-logo">
									<img src="//www.siriuscom.com/wp-content/uploads/2016/02/SiriusLogo240x79.png" width="113" height="24" alt="Sirius Computer Solutions" style="max-height: 24px; height: auto;" class="fusion-standard-logo fusion-logo-2x">
			
									<!-- mobile logo -->
															
									<img src="//www.siriuscom.com/wp-content/uploads/2016/02/SiriusLogo240x79.png" alt="Sirius Computer Solutions" class="fusion-logo-1x fusion-mobile-logo-1x">

									<img src="http://www.siriuscom.com/wp-content/uploads/2016/02/SiriusLogo240x79.png" alt="Sirius Computer Solutions" class="fusion-logo-2x fusion-mobile-logo-2x">
									
									<!-- sticky header logo -->
								</a>
								<!--
								<div class="fusion-header-content-3-wrapper">
									<div class="fusion-secondary-menu-search">
										<form role="search" class="searchform" method="get" action="http://www.siriuscom.com/">
											<div class="search-table">
												<div class="search-field">
													<input type="text" value="" name="s" class="s" placeholder="Search ...">
												</div>
												<div class="search-button">
													<input type="submit" class="searchsubmit" value="">
												</div>
											</div>
										</form>
									</div>
								</div>	
								-->	
							</div>
							<div class="fusion-mobile-menu-icons">
								<a href="#" class="fusion-icon fusion-icon-search"></a>
							</div>				
						</div>
					</div>
					<!--
					<div class="fusion-secondary-main-menu" style="top: 0px;">
						<div class="fusion-row"> -->
				
							<!-- UberMenu [Configuration:main] [Theme Loc:main_navigation] [Integration:auto] -->
							
							<!--
							<a class="ubermenu-responsive-toggle ubermenu-responsive-toggle-main ubermenu-skin-trans-black-hov ubermenu-loc-main_navigation ubermenu-responsive-toggle-content-align-left ubermenu-responsive-toggle-align-full " data-ubermenu-target="ubermenu-main-37-main_navigation">
								<i class="fa fa-bars"></i>Menu
							</a>
							<nav id="ubermenu-main-37-main_navigation" class="ubermenu ubermenu-main ubermenu-menu-37 ubermenu-loc-main_navigation ubermenu-responsive ubermenu-responsive-default ubermenu-responsive-collapse ubermenu-horizontal ubermenu-transition-shift ubermenu-trigger-hover_intent ubermenu-skin-trans-black-hov  ubermenu-bar-align-left ubermenu-items-align-left ubermenu-disable-submenu-scroll ubermenu-sub-indicators ubermenu-retractors-responsive ubermenu-notouch">
								<ul id="ubermenu-nav-main-37-main_navigation" class="ubermenu-nav">
									<li id="menu-item-12439" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-page_item ubermenu-page-item-10709 ubermenu-current_page_item ubermenu-nocurrent ubermenu-item-12439 ubermenu-item-level-0 ubermenu-column ubermenu-column-auto">
										<a class="ubermenu-target ubermenu-target-with-icon ubermenu-item-layout-default ubermenu-item-layout-icon_left ubermenu-item-notext" href="http://www.siriuscom.com/" tabindex="0"><i class="ubermenu-icon fa fa-home"></i></a></li>
										<li id="menu-item-11579" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-has-children ubermenu-item-11579 ubermenu-item-level-0 ubermenu-column ubermenu-column-natural ubermenu-has-submenu-drop ubermenu-has-submenu-flyout">
											<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/" tabindex="0"><span class="ubermenu-target-title ubermenu-target-text">About Us</span></a>
											<ul class="ubermenu-submenu ubermenu-submenu-id-11579 ubermenu-submenu-type-flyout ubermenu-submenu-drop ubermenu-submenu-align-left_edge_item ubermenu-submenu-indent">
												<li id="menu-item-11577" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11577 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-1">
													<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/executive-team/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Executive Team</span></a>
												</li>
												<li id="menu-item-11576" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11576 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-1">
													<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/in-the-news/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> In The News</span></a>
												</li>
												<li id="menu-item-11578" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11578 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-1">
													<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/careers/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Careers</span></a>
												</li>
												<li id="menu-item-13784" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-13784 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-1">
													<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/community-service-at-sirius/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Community</span></a>
												</li>
												<li id="menu-item-11575" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11575 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-1">
													<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/partners/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Partners</span></a>
												</li>
												<li class="ubermenu-retractor ubermenu-retractor-mobile">
													<i class="fa fa-times"></i> Close
												</li>
											</ul>
										</li>
										<li id="menu-item-11561" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-has-children ubermenu-advanced-sub ubermenu-item-11561 ubermenu-item-level-0 ubermenu-column ubermenu-column-auto ubermenu-has-submenu-drop ubermenu-has-submenu-mega">
											<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/" tabindex="0"><span class="ubermenu-target-title ubermenu-target-text">Solutions</span></a>
											<div class="ubermenu-submenu ubermenu-submenu-id-11561 ubermenu-submenu-type-auto ubermenu-submenu-type-mega ubermenu-submenu-drop ubermenu-submenu-align-full_width">
												<ul class="ubermenu-row ubermenu-row-id-12436 ubermenu-autoclear "></ul><ul class="ubermenu-row ubermenu-row-id-11561_auto_1 ubermenu-autoclear ">
													<li class="  ubermenu-item ubermenu-item-type-custom ubermenu-item-object-ubermenu-custom ubermenu-item-has-children ubermenu-item-12337 ubermenu-item-level-2 ubermenu-column ubermenu-column-auto ubermenu-has-submenu-stack ubermenu-item-type-column ubermenu-column-id-12337">
														<ul class="ubermenu-submenu ubermenu-submenu-id-12337 ubermenu-submenu-type-stack">
															<li id="menu-item-12866" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12866 ubermenu-item-header ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<span class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only">
																	<span class="ubermenu-target-title ubermenu-target-text"><strong>BUSINESS SOLUTIONS</strong></span>
																</span>
															</li>
															<li id="menu-item-11612" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11612 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/application-development/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Application Development</span></a>
															</li>
															<li id="menu-item-12242" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12242 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriusbrightlight.com/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Big Data &amp; Analytics</span></a>
															</li>
															<li id="menu-item-11557" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11557 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/business-continuity/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Business Continuity</span></a>
															</li>
															<li id="menu-item-11556" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11556 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/business-agility/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Business Agility</span></a>
															</li>
															<li id="menu-item-11613" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11613 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/collaboration/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Collaboration</span></a>
															</li>
															<li id="menu-item-12497" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12497 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/commerce/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Commerce</span></a>
															</li>
															<li id="menu-item-12352" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12352 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriusdigitalexperience.com/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Digital Experience</span></a>
															</li>
															<li id="menu-item-12626" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12626 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/digital-strategy-design/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Digital Strategy &amp; Design</span></a>
															</li>
															<li id="menu-item-11611" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11611 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/information-mgmt-governance/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Information Mgmt &amp; Governance</span></a>
															</li>
															<li id="menu-item-11610" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11610 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/mobile-enterprise/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Mobile Enterprise</span></a>
															</li>
															<li id="menu-item-11609" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11609 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/security-compliance/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Security &amp; Compliance</span></a>
															</li>
														</ul>
													</li>
													<li class="  ubermenu-item ubermenu-item-type-custom ubermenu-item-object-ubermenu-custom ubermenu-item-has-children ubermenu-item-12341 ubermenu-item-level-2 ubermenu-column ubermenu-column-auto ubermenu-has-submenu-stack ubermenu-item-type-column ubermenu-column-id-12341">
														<ul class="ubermenu-submenu ubermenu-submenu-id-12341 ubermenu-submenu-type-stack">
															<li id="menu-item-12865" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12865 ubermenu-item-header ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/"><span class="ubermenu-target-title ubermenu-target-text"><strong>DATA CENTER SOLUTIONS</strong></span></a>
															</li>
															<li id="menu-item-12044" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12044 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/converged-infrastructure/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Converged Infrastructure</span></a>
															</li>
															<li id="menu-item-12056" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12056 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/end-user-computing/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  End-User Computing</span></a>
															</li>
															<li id="menu-item-12055" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12055 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/networking/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Networking</span></a>
															</li>
															<li id="menu-item-12054" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12054 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/servers/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Servers</span></a>
															</li>
															<li id="menu-item-12053" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12053 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/storage/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i>  Storage</span></a>
															</li>
														</ul>
													</li>
													<li class="  ubermenu-item ubermenu-item-type-custom ubermenu-item-object-ubermenu-custom ubermenu-item-has-children ubermenu-item-12533 ubermenu-item-level-2 ubermenu-column ubermenu-column-auto ubermenu-has-submenu-stack ubermenu-item-type-column ubermenu-column-id-12533">
														<ul class="ubermenu-submenu ubermenu-submenu-id-12533 ubermenu-submenu-type-stack">
															<li id="menu-item-12857" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12857 ubermenu-item-header ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/"><span class="ubermenu-target-title ubermenu-target-text"><strong>SOLUTION SERVICES</strong></span></a>
															</li>
															<li id="menu-item-12858" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12858 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/cloud/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Cloud</span></a>
															</li>
															<li id="menu-item-12859" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12859 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/financial-services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Financial Services</span></a>
															</li>
															<li id="menu-item-12860" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12860 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/infrastructure-consulting-services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Infrastructure Consulting Services</span></a>
															</li>
															<li id="menu-item-12861" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12861 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/maintenance-asset-mgmt/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Maintenance &amp; Asset Mgmt</span></a>
															</li>
															<li id="menu-item-12862" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12862 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/managed-services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Managed Services</span></a>
															</li>
															<li id="menu-item-12863" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12863 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/project-mgmt-services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Project Mgmt Services</span></a>
															</li>
															<li id="menu-item-12864" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12864 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/software-consulting-services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Software Consulting Services</span></a>
															</li>
															<li id="menu-item-13749" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-13749 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/services/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Sirius Services Catalog</span></a>
															</li>
														</ul>
													</li>
													<li class="  ubermenu-item ubermenu-item-type-custom ubermenu-item-object-ubermenu-custom ubermenu-item-has-children ubermenu-item-12856 ubermenu-item-level-2 ubermenu-column ubermenu-column-auto ubermenu-has-submenu-stack ubermenu-item-type-column ubermenu-column-id-12856">
														<ul class="ubermenu-submenu ubermenu-submenu-id-12856 ubermenu-submenu-type-stack">
															<li id="menu-item-12538" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-12538 ubermenu-item-header ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<span class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only">
																	<span class="ubermenu-target-title ubermenu-target-text"><strong>INDUSTRY SOLUTIONS</strong></span>
																</span>
															</li>
															<li id="menu-item-12534" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12534 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/government/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Government</span></a>
															</li>
															<li id="menu-item-14183" class="ubermenu-item ubermenu-item-type-custom ubermenu-item-object-custom ubermenu-item-14183 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.force3.com/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Force 3 Federal Solutions</span></a>
															</li>
															<li id="menu-item-12537" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12537 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/healthcare/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Healthcare</span></a>
															</li>
															<li id="menu-item-12536" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12536 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/insurance/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Insurance</span></a>
															</li>
															<li id="menu-item-12535" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12535 ubermenu-item-auto ubermenu-item-normal ubermenu-item-level-3 ubermenu-column ubermenu-column-auto">
																<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/solutions/retail/"><span class="ubermenu-target-title ubermenu-target-text"><i class="fa fa-caret-right"></i> Retail</span></a>
															</li>
														</ul>
													</li>
												</ul>
												<div class="ubermenu-retractor ubermenu-retractor-mobile"><i class="fa fa-times"></i> Close</div>
											</div>
										</li>
										<li id="menu-item-11620" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-11620 ubermenu-item-level-0 ubermenu-column ubermenu-column-auto">
											<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/case-studies/" tabindex="0"><span class="ubermenu-target-title ubermenu-target-text">Case Studies</span></a>
										</li>
										<li id="menu-item-13033" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-13033 ubermenu-item-level-0 ubermenu-column ubermenu-column-auto">
											<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/blog/" tabindex="0"><span class="ubermenu-target-title ubermenu-target-text">Blog</span></a>
										</li>
										<li id="menu-item-12882" class="ubermenu-item ubermenu-item-type-post_type ubermenu-item-object-page ubermenu-item-12882 ubermenu-item-level-0 ubermenu-column ubermenu-column-auto">
											<a class="ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only" href="http://www.siriuscom.com/about-us/contact-us/" tabindex="0"><span class="ubermenu-target-title ubermenu-target-text">Contact Us</span></a>
										</li>
									</ul>
								</nav>
								<!-- End UberMenu -->
								<!--
								<div class="fusion-secondary-menu-search">
									<form role="search" class="searchform" method="get" action="http://www.siriuscom.com/">
										<div class="search-table">
											<div class="search-field">
												<input type="text" value="" name="s" class="s" placeholder="Search ...">
											</div>
											<div class="search-button">
												<input type="submit" class="searchsubmit" value="">
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					-->
					</div> <!-- end fusion sticky header wrapper -->
				</div>
				<div class="fusion-clearfix"></div>
			</div>
			
<!-- LANDING PAGE CONTENT GOES HERE -->			
			<div id="lolo-content" class="cf">
				<!-- START HERO  -->
				<div class="hero flexslider">
					<ul class="slides">
						<li class="drdelay-slide">
							<div class="hero-image">
								<div class="hero-overlay wrap cf">
									<div class="lolo-latency-logo">
										<img src="../img/dr-delay-logo.png" />
									</div>
									<div class="lolo-character">
										<img src="../img/dr-delay-character.png" />
									</div>							
									<div class="thought-bubble">
										<img class="thought-bubble-desktop" src="../img/dr-delay-thought-bubble.png" />
										<img class="thought-bubble-mobile" src="../img/dr-delay-thought-bubble-mobile.png" />
									</div>	
									<div class="speech-bubble">
										<img class="speech-bubble-desktop" src="../img/dr-delay-speech-bubble.png" />
										<img class="speech-bubble-mobile" src="../img/dr-delay-speech-bubble-mobile.png" />
									</div>												
								</div>
							</div>
							<div class="hero-content cf">
								<div class="hero-content-inner wrap">
									<h2>Dr. Delay fears the portable, scalable and flexible powers of IBM Spectrum Storage Suite</h2>
								</div>
							</div>							
						</li>
						<li class="lolo-slide">
							<div class="hero-image">
								<div class="hero-overlay wrap cf">
									<div class="lolo-latency-logo">
										<img src="../img/lolo-latency-logo.png" />
									</div>
									<div class="lolo-character">
										<img src="../img/lolo-character.png" />
									</div>							
									<div class="thought-bubble">
										<img class="thought-bubble-desktop" src="../img/thought-bubble.png" />
										<img class="thought-bubble-mobile" src="../img/thought-bubble-mobile.png" />
									</div>	
									<div class="speech-bubble">
										<img class="speech-bubble-desktop" src="../img/speech-bubble.png" />
										<img class="speech-bubble-mobile" src="../img/speech-bubble-mobile.png" />
									</div>												
								</div>
							</div>
							<div class="hero-content cf">
								<div class="hero-content-inner wrap">
									<h2>Lolo Latency Recommends <span class="hero-italic">IBM F<span>lash</span>S<span>ystem</span></span> to help large and small businesses dramatically improve the performance of their applications like a superhero.</h2>
								</div>
							</div>								
						</li>							
					</ul>

				</div>
				<!-- END HERO  -->	
				
				<div id="inner-content" class="wrap cf">
					<!-- START MAIN  -->
					<div class="main m-all t-all d-main">
						<div class="description">
							<h3>
								Defeat Dr. Delay’s inconsistent performance and reduced capacity. iBM Spectrum Storage Suite unlocks the potential of data and increases your business agility and efficiency. 
							</h3>
							<ul>
								<li><span>CONTROL </span> Analytics-driven data management to reduce costs by up to 50 percent </li>
								<li><span>PROTECT </span> Optimized data protection to reduce backup costs by up to 53 percent</li>
								<li><span>ARCHIVE </span> Simple to use data retention that reduces TCO for active archive data by up to 90%</li>
								<li><span>VIRTUALIZE </span> Virtualization of multi-vendor environments stores up to 5x more data</li>
								<li><span>ACCELERATE </span> Enterprise storage for cloud deployed in minutes instead of months</li>
								<li><span>SCALE </span> High-performance, highly scalable storage for structured and unstructured data</li>
							</ul>
							<p>Sirius is an IBM Storage Specialty Elite partner with a dedicated storage practice of more than 80 engineers who have over 570 years of combined IBM Storage experience.</p>
						</div>
						<div class="buckets cf">
							<div class="bucket-item bucket-white-paper">
								<img src="img/download-icon.png" alt="document"/>
								<h2>Want to learn more?</h2>
								<p>Download the white paper<br/><span style="font-size:12px;line-height:18px;display:inline-block;">Technology and engineering to power<br>the future of business.</span></p>
								<a class="red-btn" href="/IBMSpectrum/img/LoLo IBM Whitepaper.pdf" target="_blank">Download</a>
							</div>
							<div class="bucket-item bucket-latency-game">
								<img src="img/lightning-icon.png" alt="lightning"/>
								<h2>Test Your Latency!</h2>
								<p>Play data dash and save your score to the leaderboard for a gift from sirius!</p>
								<a class="game-btn" href="http://offers.siriuscom.com/lolo/game/" ><img src="img/start-game-btn.png" alt="start game"/></a>								
							</div>							
						</div>
					</div>
					<!-- END MAIN  -->	
					
					<!-- START SIDEBAR  -->					
					<div id="free_gift_sidebar" class="sidebar m-all t-all d-sidebar last-col">
						<div class="widget widget-free-gift">
							
							<img class="claim-free-gift" src="img/claim-free-gift.png" width="398" height="342" alt="Claim Your Free Gift">
							<img class="free-gift-arrow" src="img/sidebar-arrow.png" alt="Arrow">
							<?php if(isset($_GET['form_success']) && $_GET['form_success'] == 'true'):?>							
									
									<?php echo '<p class="thankyou">Thanks, your code has been sent!</p>';?>
									
							<?php else :?>
								
									<form id="form1" name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
										<h4>Enter Your Code Here</h4>
										<input type="text" name="giftcode" id="giftcode" placeholder="CODE" value="" /> 
										<input type="hidden" id="submitting_form" name="submitting_form" value="1">
										<input class="red-btn" type="submit" value="Submit">
									</form>									
								
							<?php endif;?>				
							
							<div class="form-description">
								<h4>Watch Your Mail For</h4>
								<img class="book-icon" src="img/clock-icon.png" alt="Book Icon">
								<p>
									<strong>Comic Book Cover Wall Clock</strong><br/>
									10” diameter<br/> 
								</p>
							</div>
							
						</div>
					</div>
					<!-- END SIDEBAR  -->
					
					<!-- START PRE-FOOTER (action-lolo)  -->		
					<div class="action-lolo cf">
						<div class="action-lolo-character">
							<img src="../img/action-lolo.png" />
						</div>	
						<div class="action-speech-bubble">
							<img src="../img/action-speech-bubble.png" />
						</div>
						<div class="action-crowd-bubble">
							<img src="../img/action-crowd-bubble.png" />
						</div>	
																				
					</div>
					<!-- END PRE-FOOTER (action-lolo)  -->	
				</div><!-- END #inner-content  -->	
			</div>
<!-- END LANDING PAGE CONTENT -->			
			
			<div class="fusion-footer">						
				<footer class="fusion-footer-widget-area fusion-widget-area" style="position: relative; z-index: 3;">
					<div class="fusion-row">
						<div class="fusion-columns fusion-columns-3 fusion-widget-area">
							<div class="fusion-column col-lg-3 col-md-3 col-sm-3">
								<div class="footer-logo">
									<img src="../img/sirius-footer-logo.png" alt="Sirius Computer Solutions"/>
								</div>
							</div>
							<div class="fusion-column col-lg-3 col-md-3 col-sm-3">
								<div class="footer-logo ibm-footer-logo">
									<img src="../img/ibm-footer-logo.png" alt="IBM - Premier Business Partner"/>
								</div>
							</div>							
						</div>						
						<!--
						<div class="fusion-columns fusion-columns-3 fusion-widget-area">
							<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
								<div id="search-2" class="fusion-footer-widget-column widget widget_search">
									<form role="search" class="searchform" method="get" action="http://www.siriuscom.com/">
										<div class="search-table">
											<div class="search-field">
												<input type="text" value="" name="s" class="s" placeholder="Search ...">
											</div>
											<div class="search-button">
												<input type="submit" class="searchsubmit" value="">
											</div>
										</div>
									</form>
									<div style="clear:both;"></div>
								</div>
							</div>
							<div class="fusion-column col-lg-4 col-md-4 col-sm-4">
								<div id="nav_menu-4" class="fusion-footer-widget-column widget widget_nav_menu">
									<div class="menu-footer-container">
										<ul id="menu-footer" class="menu">
											<li id="menu-item-13817" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13817">
												<a href="http://www.siriuscom.com/about-us/careers/">Careers</a>
											</li>
											<li id="menu-item-11962" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11962">
												<a href="http://www.siriuscom.com/about-us/contact-us/">Contact Us</a>
											</li>
											<li id="menu-item-11808" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11808">
												<a href="http://www.siriuscom.com/about-us/sirius-connect-newsletter/">Newsletter</a>
											</li>
											<li id="menu-item-11806" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11806">
												<a href="http://www.siriuscom.com/about-us/legal/">Legal</a>
											</li>
											<li id="menu-item-11809" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11809">
												<a href="http://www.siriuscom.com/about-us/accessibility/">Accessibility</a>
											</li>
											<li id="menu-item-11810" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-11810">
												<a href="http://www.siriuscom.com/about-us/eeodiversity/">EEO/Diversity</a>
											</li>
											<li id="menu-item-13185" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13185">
												<a target="_blank" href="http://salescenter.siriuscom.com">Employee Login</a>
											</li>
										</ul>
									</div>
								<div style="clear:both;"></div>
								</div>
							</div>
							<div class="fusion-column fusion-column-last col-lg-4 col-md-4 col-sm-4">	
								<div id="recent-posts-6" class="fusion-footer-widget-column widget widget_recent_entries">
									<h4 class="widget-title" data-fontsize="14" data-lineheight="14">In The News</h4>		
									<ul>
										<li>
											<a href="http://www.siriuscom.com/2016/05/hey-big-data-whats-problem/">Hey Big Data, what’s your problem?!</a>
										</li>
										<li>
											<a href="http://www.siriuscom.com/2016/05/whats-right-enterprise-mobility-management-suite/">What’s the Right Enterprise Mobility Management Suite for You?</a>
										</li>
										<li>
											<a href="http://www.siriuscom.com/2016/05/sirius-computer-solutions-enters-business-transition-agreement-northwind-consulting-services-llc/">Sirius Computer Solutions Enters Into A Business Transition Agreement With NorthWind Consulting Services, LLC</a>
										</li>
									</ul>
									<div style="clear:both;"></div>
								</div>		
							</div>
							<div class="fusion-clearfix"></div>
						</div>
						-->
						 <!-- fusion-columns -->
					</div> <!-- fusion-row -->
				</footer> <!-- fusion-footer-widget-area -->
				<footer id="footer" class="fusion-footer-copyright-area" style="position: relative; z-index: 3;">
					<div class="fusion-row">
						<div class="fusion-copyright-content">
							<div class="fusion-copyright-notice">
								<div>Copyright © Sirius Computer Solutions. All rights reserved.<br>Powered by Sirius Digital Strategy &amp; Design and Sirius Cloud. Support by Sirius Managed Services.</div>
							</div>
							<div class="fusion-social-links-footer">
								<div class="fusion-social-networks">
									<div class="fusion-social-networks-wrapper">
										<a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#ffffff;" target="_blank" href="http://www.facebook.com/siriuscomputersolutions" data-placement="top" data-title="Facebook" data-toggle="tooltip" title="" data-original-title="Facebook"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#ffffff;" target="_blank" href="https://twitter.com/SiriusNews" data-placement="top" data-title="Twitter" data-toggle="tooltip" title="" data-original-title="Twitter"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube" style="color:#ffffff;" target="_blank" href="http://www.youtube.com/siriusnewscast" data-placement="top" data-title="Youtube" data-toggle="tooltip" title="" data-original-title="Youtube"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-googleplus fusion-icon-googleplus" style="color:#ffffff;" target="_blank" href="https://plus.google.com/u/0/b/112331788911174015967/112331788911174015967/posts" data-placement="top" data-title="Google+" data-toggle="tooltip" title="" data-original-title="Google+"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin fusion-last-social-icon" style="color:#ffffff;" target="_blank" href="http://www.linkedin.com/company/sirius-computer-solutions" data-placement="top" data-title="Linkedin" data-toggle="tooltip" title="" data-original-title="Linkedin"></a>
									</div>
								</div>			
							</div>
		
						</div> <!-- fusion-fusion-copyright-content -->
					</div> <!-- fusion-row -->
				</footer> <!-- #footer -->
			</div>

		<script type="text/javascript">
		/* <![CDATA[ */
		var js_local_vars = {"admin_ajax":"http:\/\/www.siriuscom.com\/wp-admin\/admin-ajax.php","admin_ajax_nonce":"10a99139e1","protocol":"","theme_url":"http:\/\/www.siriuscom.com\/wp-content\/themes\/Avada","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","page_smoothHeight":"false","flex_smoothHeight":"false","language_flag":"","infinite_blog_finished_msg":"<em>All posts displayed.<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","portfolio_loading_text":"<em>Loading Portfolio Items...<\/em>","faqs_loading_text":"<em>Loading FAQ Items...<\/em>","order_actions":"Details","avada_rev_styles":"1","avada_styles_dropdowns":"1","blog_grid_column_spacing":"40","blog_pagination_type":"Pagination","body_font_size":"14","carousel_speed":"2500","content_break_point":"800","custom_icon_image_retina":"","disable_mobile_animate_css":"0","disable_mobile_image_hovers":"1","portfolio_pagination_type":"Pagination","form_bg_color":"#ffffff","header_transparency":"0","header_padding_bottom":"0px","header_padding_top":"0px","header_position":"Top","header_sticky":"1","header_sticky_tablet":"0","header_sticky_mobile":"0","header_sticky_type2_layout":"menu_only","sticky_header_shrinkage":"1","is_responsive":"1","is_ssl":"false","isotope_type":"masonry","layout_mode":"wide","lightbox_animation_speed":"Normal","lightbox_arrows":"1","lightbox_autoplay":"0","lightbox_behavior":"all","lightbox_desc":"1","lightbox_deeplinking":"1","lightbox_gallery":"1","lightbox_opacity":"0.9","lightbox_path":"vertical","lightbox_post_images":"1","lightbox_skin":"metro-white","lightbox_slideshow_speed":"5000","lightbox_social":"1","lightbox_title":"1","lightbox_video_height":"720","lightbox_video_width":"1280","logo_alignment":"Left","logo_margin_bottom":"0px","logo_margin_top":"0px","megamenu_max_width":"1100px","mobile_menu_design":"modern","nav_height":"40","nav_highlight_border":"3","page_title_fading":"0","pagination_video_slide":"0","related_posts_speed":"2500","retina_icon_height":"","retina_icon_width":"","submenu_slideout":"1","side_header_break_point":"1023","sidenav_behavior":"Hover","site_width":"1100px","slider_position":"below","slideshow_autoplay":"1","slideshow_speed":"7000","smooth_scrolling":"1","status_lightbox":"0","status_totop_mobile":"0","status_vimeo":"0","status_yt":"0","testimonials_speed":"4000","tfes_animation":"sides","tfes_autoplay":"1","tfes_interval":"3000","tfes_speed":"800","tfes_width":"150","title_style_type":"double","typography_responsive":"0","typography_sensitivity":"0.6","typography_factor":"1.5","woocommerce_shop_page_columns":"4","side_header_width":"0"};
		/* ]]> */
		</script>        
		<script type="text/javascript" src="http://www.siriuscom.com/wp-content/themes/Avada/assets/js/main.min.js?ver=1.0.0" async=""></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		var ubermenu_data = {"remove_conflicts":"on","reposition_on_load":"off","intent_delay":"300","intent_interval":"100","intent_threshold":"7","scrollto_offset":"50","scrollto_duration":"1000","responsive_breakpoint":"959","accessible":"on","retractor_display_strategy":"responsive","touch_off_close":"on","v":"3.2.1.1","configurations":["main"],"ajax_url":"http:\/\/www.siriuscom.com\/wp-admin\/admin-ajax.php"};
		/* ]]> */
		</script>		
        <script type="text/javascript" src="http://www.siriuscom.com/wp-content/plugins/ubermenu/assets/js/ubermenu.min.js?ver=3.2.1.1"></script>
        
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
